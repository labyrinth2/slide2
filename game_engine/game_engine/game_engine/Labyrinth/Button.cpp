#include "Button.h"



Button::Button(slide::Board * pbRef, const core::math::vec2 & ppos, core::graphics::Sprite * ptex, int pbuttonVal) {

	bRef = pbRef;
	buttonVal = pbuttonVal;
	pos = ppos;
	boundingBox = core::math::vec4(pos.x, pos.y, BUTTON_WIDTH, BUTTON_HEIGHT);
	tex = ptex;

	core::interact::Interactable::interacts.push_back(this);


}

Button::~Button() {

}


void Button::draw(SDL_Renderer* renderer) {
	tex->renderSprite(pos.x, pos.y);
}

void Button::onClick(slide::Player* player) {
	bRef->extra->rotationValue = (bRef->extra->rotationValue + buttonVal) % 4;
	if (bRef->extra->rotationValue < 0)
		bRef->extra->rotationValue = 3;
	bRef->extra->swapTex();
}

