#include "Tile.h"


namespace slide {

	core::graphics::Sprite** Tile::textures;

	Tile::Tile(const core::math::vec2& pboardPos, const core::math::vec2& ppos, const core::math::vec2& psize, int ptileID, int protVal)
		: size(psize) 
	{

		boardPos = pboardPos;
		pos = ppos;
		boundingBox = core::math::vec4(pos.x, pos.y, TILE_WIDTH, TILE_HEIGHT);
		tileID = ptileID;
		rotationValue = protVal;
		swapTex();
		genWalls();
		core::interact::Interactable::interacts.push_back(this);

	}

	Tile::~Tile() {
		
	}

	void Tile::draw(SDL_Renderer* renderer) {
		texture->renderSprite(pos.x, pos.y);
		if (afact != nullptr)
			afact->texture->renderSprite(pos.x + ((TILE_WIDTH/2) - (ARTIFACT_WIDTH/2)), pos.y + ((TILE_HEIGHT / 2) - (ARTIFACT_HEIGHT / 2)));
	}

	void Tile::onClick(Player* p) {

		p->setPos(core::math::vec2((this->pos.x + (TILE_WIDTH/2))-(PLAYER_WIDTH/2), (this->pos.y + (TILE_HEIGHT/2))-(PLAYER_HEIGHT/2)));
		if (afact != nullptr) {
			//we can check here later if the player is looking for this afact***
			p->inventory.push_back(afact);
			afact = nullptr;
		}

	}

	void Tile::genWalls() {
		switch (tileID) {
		case 0:
			walls[0] = 0;
			walls[1] = 1;
			walls[2] = 0;
			walls[3] = 1;
		case 1:
			walls[0] = 0;
			walls[1] = 1;
			walls[2] = 1;
			walls[3] = 0;
		case 2:
			walls[0] = 1;
			walls[1] = 1;
			walls[2] = 0;
			walls[3] = 1;
		default:
			walls[0] = 0;
			walls[1] = 0;
			walls[2] = 0;
			walls[3] = 0;
		}
	}

	void Tile::swapTex() {
		texture = Tile::textures[(tileID * 4) + (rotationValue % 4)];
	}
	
	void Tile::setTexArray(core::graphics::Sprite** texArray) {
		Tile::textures = texArray;
	}

}