#include "Artifact.h"

namespace slide {

	Artifact::Artifact(core::graphics::Sprite* ptexture) {
		//pos = ppos;
		texture = ptexture;
		id = genID();

	}

	Artifact::~Artifact() { }

	void Artifact::setPos(const core::math::vec2& new_pos) {
		pos = new_pos;
	}

	void Artifact::draw(SDL_Renderer* renderer) {
		texture->renderSprite((int)pos.x, (int)pos.y);
	}

	unsigned int Artifact::genID() {
		static int last = -1;
		return ++last;
	}

	Artifact* Artifact::makeUniqueArtifact(SDL_Renderer* rend) {
		Artifact* out;
		static bool isFirstCall = true;

		if (isFirstCall) {//after the fist call disable seeding
			isFirstCall = false;
			srand((unsigned)time(NULL));
		}

		static bool imgIds[24] = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };

		int currid;

		do {
			currid = rand() % 24; //BUG_REPORT: this WILL cause a inf loop on the 25th afact
		} while (imgIds[currid]);
		imgIds[currid] = true;

		core::graphics::Sprite* tempTex = new core::graphics::Sprite(rend, std::string("res/Artifacts/artifact" + std::to_string(currid) + ".png"), NULL);

		out = new Artifact(tempTex);
		return out;
	}

}