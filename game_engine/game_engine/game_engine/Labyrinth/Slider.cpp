#include "Slider.h"

namespace slide {

	Slider::Slider(const core::math::vec2& ppos, core::graphics::Sprite* ptexture, Board* pboardRef, Tile* phead, int pside) {

		pos = ppos;
		texture = ptexture;
		boardRef = pboardRef;
		head = phead;
		side = pside;

		boundingBox = core::math::vec4(pos.x, pos.y, SLIDER_WIDTH, SLIDER_HEIGHT);

		core::interact::Interactable::interacts.push_back(this);

	}

	Slider::~Slider() {
		
	}

	void Slider::draw(SDL_Renderer* renderer) {
		texture->renderSprite((int)pos.x, (int)pos.y);
	}

	void Slider::onClick(Player* p) {
		boardRef->slideTiles(head, side);
	}

}