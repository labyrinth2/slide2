#pragma once 
#include <SDL2/SDL.h>
#include "../src/Graphics/Renderable.h"
#include "../src/Graphics/Window.h"
#include "Tile.h"
#include "../src/Graphics/Sprite.h"
#include "../src/Math/vec2.h"
#include "../src/Math/vec4.h"
#include "../src/Interactables/Interactable.h"
#include <vector>





namespace slide {

	class Board : public core::graphics::Renderable {
	public:

		core::graphics::Window* win;
		SDL_Renderer* renderer;
		int size; //size of one side
		std::vector<Tile*> tiles;
		std::vector<Tile*> sliderTiles;
		Tile* extra;

		Board(core::graphics::Window* pwin, SDL_Renderer* rend, const int& psize);
		~Board();

		void draw(SDL_Renderer* renderer);
		core::interact::Interactable* getTileOnClick(core::math::vec2 point);

		void slideTiles(Tile* head, int side);
		bool isTileConected(const core::math::vec2& startTilePos, const core::math::vec2& endTilePos);

		core::math::vec2 centerBoard();
		void update();

	private:

		
		std::string getClip(int n);
		int getTileIndex(Tile* t);
		int getVectorConversion(const core::math::vec2& pos);
		bool wallIntersectionsMatch(int currPos, int nbor, int sideOfWall);
		


	};

}

