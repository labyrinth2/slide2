#include "Board.h"
#include <stdlib.h>
#include <time.h>
#include "Slider.h"

namespace slide {


	Board::Board(core::graphics::Window* pwin, SDL_Renderer* rend, const int& psize)
		: size(psize)
	{
		win = pwin;
		renderer = rend;
		pos = centerBoard();

		srand((unsigned int)time(NULL));
		int num = 0;
		int rot = 0;

		tiles = std::vector<Tile*>();
		for (int j = 0; j < size; ++j) {
			for (int i = 0; i < size; ++i) {
				num = rand() % 3;
				rot = rand() % 4;

				Tile* tile = new Tile(core::math::vec2(i, j), core::math::vec2(pos.x + (i * (TILE_WIDTH + SPACER)), pos.y + (j * (TILE_HEIGHT + SPACER))), core::math::vec2(TILE_WIDTH, TILE_HEIGHT), num, rot);

				tiles.push_back(tile); //do not delete tile
			}
		}
		num = rand() % 3;
		rot = rand() % 4;
		extra = new Tile(core::math::vec2(-1, -1), core::math::vec2(win->getWidth() - TILE_WIDTH, 0), core::math::vec2(TILE_WIDTH, TILE_HEIGHT), num, rot);

	}

	std::string Board::getClip(int n) {
		
		switch (n) {
		case 0:
			return "0";
		case 1:
			return "1";
		case 2:
			return "2";
		case 3:
			return "3";
		case 4:
			return "4";
		case 5: 
			return "5";
		case 6:
			return "6";
		default:
			return "-1";
		} 

	}

	Board::~Board() {
		for (int i = 0; i < tiles.size(); ++i) {
			delete tiles[i];
		}
		delete extra;
	}

	void Board::draw(SDL_Renderer* renderer) {
		for (int i = 0; i < (size * size); ++i) {
			tiles[i]->draw(renderer);
		}
		extra->draw(renderer);
	}

	void Board::slideTiles(Tile* head, int side) {
		Tile* iter = head; //iterates over the row/column
		Tile popped;
		int index = getTileIndex(head);
		int currindex = 0;
		switch (side) {
		case SIDE_TOP:   //NOTE: YOU NEED TO DEEP COPY THE TILES TO MOVE THEM! SHIFT THE (POS) AND (BOUNDING BOX)!!!
			//we need to slide down
			//index has to be 1,3,5
			currindex = (6 * size) + index;
			iter = tiles[currindex]; //last tile in col
			popped.pos = iter->pos; //copy its data
			popped.boundingBox = iter->boundingBox;
			popped.texture = iter->texture;
			popped.afact = iter->afact;
			popped.tileID = iter->tileID;
			popped.rotationValue = iter->rotationValue;
			for (int i = 5; i >= 0; --i) {
				tiles[currindex]->texture = tiles[i * size + index]->texture;
				tiles[currindex]->afact = tiles[i * size + index]->afact;
				tiles[currindex]->tileID = tiles[i * size + index]->tileID;
				tiles[currindex]->rotationValue = tiles[i * size + index]->rotationValue;
				currindex = (i*size) + index;
			}
			head->texture = extra->texture;
			head->afact = extra->afact;
			head->tileID = extra->tileID;
			head->rotationValue = extra->rotationValue;
			extra->texture = popped.texture;
			extra->afact = popped.afact;
			extra->tileID = popped.tileID;
			extra->rotationValue = popped.rotationValue;
			break;
		case SIDE_RIGHT:
			//we need to slide left
			currindex = index - (size - 1);
			iter = tiles[currindex]; //last tile in col
			popped.pos = iter->pos; //copy its data
			popped.boundingBox = iter->boundingBox;
			popped.texture = iter->texture;
			popped.afact = iter->afact;
			popped.tileID = iter->tileID;
			popped.rotationValue = iter->rotationValue;
			for (int i = 0; i < size-1; ++i) {
				tiles[currindex]->texture = tiles[currindex + 1]->texture;
				tiles[currindex]->afact = tiles[currindex + 1]->afact;
				tiles[currindex]->tileID = tiles[currindex + 1]->tileID;
				tiles[currindex]->rotationValue = tiles[currindex + 1]->rotationValue;
				currindex = currindex + 1;
			}
			head->texture = extra->texture;
			head->afact = extra->afact;
			head->tileID = extra->tileID;
			head->rotationValue = extra->rotationValue;
			extra->texture = popped.texture;
			extra->afact = popped.afact;
			extra->tileID = popped.tileID;
			extra->rotationValue = popped.rotationValue;
			break;
		case SIDE_BOTTOM:
			//we need to slide up
			index = index - 42; //to get 1,3,5 (aka top side)
			currindex = index;
			iter = tiles[index]; //last tile in col
			popped.pos = iter->pos; //copy its data
			popped.boundingBox = iter->boundingBox;
			popped.texture = iter->texture;
			popped.afact = iter->afact;
			popped.tileID = iter->tileID;
			popped.rotationValue = iter->rotationValue;
			for (int i = 1; i < size; ++i) {
				tiles[currindex]->texture = tiles[i * size + index]->texture;
				tiles[currindex]->afact = tiles[i * size + index]->afact;
				tiles[currindex]->tileID = tiles[i * size + index]->tileID;
				tiles[currindex]->rotationValue = tiles[i * size + index]->rotationValue;
				currindex = i * size + index;
			}
			head->texture = extra->texture;
			head->afact = extra->afact;
			head->tileID = extra->tileID;
			head->rotationValue = extra->rotationValue;
			extra->texture = popped.texture;
			extra->afact = popped.afact;
			extra->tileID = popped.tileID;
			extra->rotationValue = popped.rotationValue;
			break;
		case SIDE_LEFT:
			//we need to slide right
			currindex = index + (size - 1);//opp side
			iter = tiles[currindex];
			popped.pos = iter->pos; //copy its data
			popped.boundingBox = iter->boundingBox;
			popped.texture = iter->texture;
			popped.afact = iter->afact;
			popped.tileID = iter->tileID;
			popped.rotationValue = iter->rotationValue;
			for (int i = 5; i >= 0; --i) {
				tiles[currindex]->texture = tiles[currindex - 1]->texture;
				tiles[currindex]->afact = tiles[currindex-1]->afact;
				tiles[currindex]->tileID = tiles[currindex - 1]->tileID;
				tiles[currindex]->rotationValue = tiles[currindex - 1]->rotationValue;
				currindex = currindex-1;
			}
			head->texture = extra->texture;
			head->afact = extra->afact;
			head->tileID = extra->tileID;
			head->rotationValue = extra->rotationValue;
			extra->texture = popped.texture;
			extra->afact = popped.afact;
			extra->tileID = popped.tileID;
			extra->rotationValue = popped.rotationValue;
			break;
		}
	}

	core::interact::Interactable* Board::getTileOnClick(core::math::vec2 point) {
		return core::interact::Interactable::getOnClick(point);
	}

	core::math::vec2 Board::centerBoard() {
		int wid = (TILE_WIDTH * size) + (size * SPACER);
		int hgt = (TILE_HEIGHT * size) + (size * SPACER);
		
		return core::math::vec2((float)((win->getWidth() / 2) - (wid / 2)), (float)((win->getHeight() / 2) - (hgt / 2)));

	}

	void Board::update() {
		for (int j = 0; j < size; ++j) {//vert
			for (int i = 0; i < size; ++i) {//horz
				tiles[(size * j) + i]->pos = core::math::vec2(pos.x + (i * (TILE_WIDTH + SPACER)), pos.y + (j * (TILE_HEIGHT + SPACER)));
				tiles[(size * j) + i]->boundingBox = core::math::vec4(tiles[(size * j) + i]->pos.x, tiles[(size * j) + i]->pos.y, TILE_WIDTH, TILE_HEIGHT);
			}
		}
	}

	int Board::getTileIndex(Tile* t) {
		int index = 0;
		Tile* iter = tiles[index];
		while (iter != nullptr || index < (size * size)) {
			if (iter == t) {
				return index;
			}
			else {
				index++;
				iter = tiles[index];
			}
		}
	}

	bool Board::isTileConected(const core::math::vec2& startTilePos, const core::math::vec2& endTilePos) {
		std::vector<int> visited = std::vector<int>();//points visited (also the connected points)
		std::vector<int> openSet = std::vector<int>();//points to visit
		int currentPos;
		int endPos = getVectorConversion(endTilePos);
		int up, right, down, left; //temps
		openSet.push_back(getVectorConversion(startTilePos)); //add the start tile to the openSet
		while (!openSet.empty()) {
			currentPos = openSet.back();//grab a point out of the openSet
			openSet.pop_back();//remove the point
			visited.push_back(currentPos);//mark it as connected
			//get the connected neighbors of the point
			up = currentPos - size;
			right = currentPos + 1;
			down = currentPos + size;
			left = currentPos - 1;
			if (up >= 0 && wallIntersectionsMatch(currentPos, up, TILE_SIDE_TOP)) {
				openSet.push_back(up);
			}
			if (right < tiles.size() && wallIntersectionsMatch(currentPos, right, TILE_SIDE_RIGHT)) {
				openSet.push_back(right);
			}
			if (down < tiles.size() && wallIntersectionsMatch(currentPos, down, TILE_SIDE_BOTTOM)) {
				openSet.push_back(down);
			}
			if (left >= 0 && wallIntersectionsMatch(currentPos, left, TILE_SIDE_LEFT)) {
				openSet.push_back(left);
			}
		}
		
		for (int i = 0; i < visited.size(); ++i) {
			if (visited[i] == endPos) {
				return true;
			}
		}

		return false;

	}

	int Board::getVectorConversion(const core::math::vec2& pos) {
		return (int)((pos.y * size) + pos.x);
	}

	bool Board::wallIntersectionsMatch(int currPos, int nbor, const int sideOfMatch) {
		//wallActual = (wallIndex + rotVal) % 4;
		return (tiles[currPos]->walls[sideOfMatch] && tiles[nbor]->walls[(sideOfMatch + 2) % 4]);
	}




}