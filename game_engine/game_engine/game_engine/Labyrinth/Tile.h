#pragma once
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "Player.h"
#include "../src/Graphics/Renderable.h"
#include "../src/Interactables/Interactable.h"

#define TILE_WIDTH   100
#define TILE_HEIGHT  100
#define SPACER       5

#define TILE_SIDE_TOP    0
#define TILE_SIDE_RIGHT  1
#define TILE_SIDE_BOTTOM 2
#define TILE_SIDE_LEFT   3


namespace slide {
	 
	class Tile : public core::graphics::Renderable, public core::interact::Interactable {
	public:

		core::math::vec2 size;
		core::graphics::Sprite* texture;
		Player* player;
		Artifact* afact;
		int tileID;
		int rotationValue;
		core::math::vec2 boardPos = core::math::vec2(0, 0);
		bool walls[4] = { 0, 0, 0, 0 }; //up, right, down, left (0 = wall, 1 = path)
		static core::graphics::Sprite** textures;
		
		Tile() = default;
		Tile(const core::math::vec2& pboardPos, const core::math::vec2& ppos, const core::math::vec2& psize, int ptileID, int protVal);
		~Tile();

		void draw(SDL_Renderer* renderer);
		void onClick(Player* p);

		void swapTex();
		static void setTexArray(core::graphics::Sprite** texArray);

	private:

		void genWalls();
		
	
	};

}
