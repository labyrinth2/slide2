#include "Player.h"

namespace slide {


	Player::Player(const core::math::vec2& ppos, core::graphics::Sprite* ptexture) {
		pos = ppos;
		texture = ptexture;
		inventory = std::vector<Artifact*>();
	}

	Player::~Player() { }

	inline const core::math::vec2& Player::getPos() const { return pos; }

	void Player::setPos(const core::math::vec2& new_pos) {
		pos = new_pos;
	}

	void Player::setTex(core::graphics::Sprite* sp) {
		texture = sp;
	}

	void Player::draw(SDL_Renderer* renderer) {
		texture->renderSprite((int)pos.x, (int)pos.y);
		for (int i = 0; i < inventory.size(); ++i) {
			inventory[i]->texture->renderSprite((int)0, (int)i * 30); //NOTE: if the inventory is big enough the sprites an render off screen
		}
		
	}

}