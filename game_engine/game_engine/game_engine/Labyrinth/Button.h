#pragma once
#include "../src/Graphics/Renderable.h"
#include "../src/Interactables/Interactable.h"
#include "Board.h"

#define BUTTON_WIDTH   50
#define BUTTON_HEIGHT  50

class Button : public core::graphics::Renderable, public core::interact::Interactable {
private:

	core::graphics::Sprite* tex; //Button Texture
	slide::Board* bRef;
	int buttonVal;

public:

	Button(slide::Board* pbRef, const core::math::vec2& ppos, core::graphics::Sprite* ptex, int pbuttonVal);
	~Button();

	void draw(SDL_Renderer* renderer);
	void onClick(slide::Player*);


private:




};