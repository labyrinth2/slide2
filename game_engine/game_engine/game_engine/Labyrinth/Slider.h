#pragma once
#include <SDL2/SDL.h>
#include "../src/Interactables/Interactable.h"
#include "../src/Graphics/Renderable.h"
#include "../src/Graphics/Sprite.h"
#include "Board.h"
#include "Tile.h"
#include "Player.h"

#define SLIDER_WIDTH  30
#define SLIDER_HEIGHT 30

#define SIDE_TOP     0
#define SIDE_RIGHT   1
#define SIDE_BOTTOM  2
#define SIDE_LEFT    3

namespace slide {

	class Slider : public core::graphics::Renderable,  core::interact::Interactable {
	public:

		core::graphics::Sprite* texture;
		Board* boardRef;
		Tile* head;
		int side; //0 = top, 1 = right, 2 = bottom, 3 = left
		
		Slider() = default;
		Slider(const core::math::vec2& ppos, core::graphics::Sprite* ptextop, Board* pboardRef, Tile* phead, int pside);
		~Slider();

		void draw(SDL_Renderer* renderer);
		void onClick(Player* p);


	};

}