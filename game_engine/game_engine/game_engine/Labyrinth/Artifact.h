#pragma once
#include <SDL2/SDL.h>
#include "../src/Graphics/Renderable.h"
#include "../src/Math/vec2.h"
#include "../src/Math/vec4.h"
#include "../src/Graphics/Sprite.h"
#include <vector>
#include <time.h>
#include <random>

#define ARTIFACT_WIDTH 30
#define ARTIFACT_HEIGHT 30

namespace slide {

	class Artifact : public core::graphics::Renderable {
	private:

		unsigned int id;
	
	public:

		core::graphics::Sprite* texture;

		Artifact() = default;

		Artifact(core::graphics::Sprite* ptexture);

		~Artifact();

		void setPos(const core::math::vec2& new_pos);

		inline const unsigned int& getID() const { return id; }

		void draw(SDL_Renderer* renderer);

		static Artifact* makeUniqueArtifact(SDL_Renderer* rend);

	private: 

		unsigned int genID();

	};
}