#pragma once
#include "../src/Graphics/Renderable.h"
#include "../src/Graphics/Sprite.h"
#include "../src/Math/vec2.h"
#include "../src/Math/vec4.h"
#include "Artifact.h"
#include <vector>

#define PLAYER_WIDTH 60
#define PLAYER_HEIGHT 60


namespace slide {

	class Player : public core::graphics::Renderable {
	private:
	

	public:
		core::graphics::Sprite* texture;

		std::vector<Artifact*> inventory;
		

		Player(const core::math::vec2& ppos, core::graphics::Sprite* ptexture);

		~Player();

		inline const core::math::vec2& getPos() const ;

		void setPos(const core::math::vec2& new_pos);
		void setTex(core::graphics::Sprite* sp);



		void draw(SDL_Renderer* renderer);

	};

}
