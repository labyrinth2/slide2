//Lib Includes
//#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

//Core::Graphics Includes
#include "src/Graphics/Window.h"
#include "src/Renderables/Rect.h"
#include "src/Graphics/Renderer.h"
#include "src/Graphics/Sprite.h"

//Core::Interact Includes
#include "src/Interactables/Interactable.h"

//Core::Sound Includes
#include "src/Sound/Audio.h"

//Core::Math Includes
#include "src/Math/vec2.h"
#include "src/Math/vec3.h"
#include "src/Math/vec4.h"
#include "src/Math/mat4.h"

//Slide Includes
#include "Labyrinth/Player.h"
#include "Labyrinth/Board.h"
#include "Labyrinth/Slider.h"
#include "Labyrinth/Button.h"

//std Includes
#include <iostream>
#include <vector>
#include <random>
#include <time.h>

//#define's
#define WIN_WIDTH  (int)(1240 * 1.2) //1488
#define WIN_HEIGHT (int)(640  * 1.3) //832

#define ENABLED 1
#define DISABLED 0

#define AUTO_CLICKER DISABLED

using namespace slide;
using namespace core;
using namespace graphics;
using namespace math;
using namespace interact;
using namespace sound;

void key_press(Window& window, const SDL_Event& event);
void button_press(Window& window, const SDL_Event& event);
void mouse_move(Window& window, const SDL_Event& event);
void window_event(Window& window, const SDL_Event& event);

vec2 getSliderPos(int n, int i);
int  getSliderSide(int n, int i);
int mapSliderToTile(int i);

Player* player;
Board* b;
Audio* soundEng;
Slider** sliders;

Sprite* tex[4];
int main_index;

int main() {

	srand((unsigned)time(NULL));


	Window* win = new Window("Labyrinth", WIN_WIDTH, WIN_HEIGHT);
	Audio* mm = new Audio();
	Renderer* ren = new Renderer(win->getSDL_WindowPtr());

	Sprite* tile_textures[4 * 3];
	//gen tile textures
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 4; ++j) {
			tile_textures[(i * 4) + j] = new Sprite(ren->getSDL_Renderer(), std::string("res/100x100/tile" + std::to_string(i) + "/" + std::to_string(i) + "-" + std::to_string(j) + ".png"), NULL);
		}
	}
	Tile::setTexArray(tile_textures);


	Board board(win, ren->getSDL_Renderer(), 7);
	b = &board;
	soundEng = mm;

	Slider** slider = new Slider*[(board.size * 2) - 2];

	for (int i = 0; i < (board.size * 2) - 2; ++i) {
		slider[i] = new Slider(getSliderPos(board.size, i),
			new Sprite(ren->getSDL_Renderer(),
				std::string("res/Sliders/basicSlider" + std::to_string(getSliderSide(board.size, i)) + ".png"),
				NULL),
			&board,
			(board.tiles[mapSliderToTile(i)]), //NOTE: this function needs to be fixed it is only hard coded for now
			getSliderSide(board.size, i));
	}
	sliders = slider;
	
	bool invID[49] = { false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false };
	int currid;
	for (int i = 0; i < 24; ++i) {

		do {
			currid = rand() % 49; //BUG_REPORT: this WILL cause a inf loop on the 25th afact
		} while (invID[currid]);
		invID[currid] = true;

		board.tiles[currid]->afact = Artifact::makeUniqueArtifact(ren->getSDL_Renderer());
			
	}


	//note: this is only a test but there is no delete segment for these allocations
	Sprite* playerTexture = new Sprite(ren->getSDL_Renderer(), std::string("res/Player/60x60/Wizard-blue.png"), NULL);
	Sprite* redtex = new Sprite(ren->getSDL_Renderer(), std::string("res/Player/60x60/Wizard-red.png"), NULL);
	Sprite* greentex = new Sprite(ren->getSDL_Renderer(), std::string("res/Player/60x60/Wizard-green.png"), NULL);
	Sprite* purptex = new Sprite(ren->getSDL_Renderer(), std::string("res/Player/60x60/Wizard-purple.png"), NULL);

	//test to make sure all textures for player work
	tex[0] = playerTexture;
	tex[1] = redtex;
	tex[2] = greentex;
	tex[3] = purptex;

	Player bob = Player(vec2(30, 30), playerTexture);
	player = &bob;

	Sprite* bg = new Sprite(ren->getSDL_Renderer(), std::string("res/bg/tablebg.png"), NULL);


	win->setKeyCallback(key_press);
	win->setMouseButtonCallback(button_press);
	win->setMouseMotionCallback(mouse_move);
	win->setWindowEventCallback(window_event);

	mm->playMusic();

	bob.setPos(vec2(board.pos.x + ((TILE_WIDTH/2) - (PLAYER_WIDTH/2)), board.pos.y + ((TILE_HEIGHT / 2) - (PLAYER_HEIGHT / 2))));
	
	Button rotButton = Button(&board, vec2(board.extra->pos.x + 25, board.extra->pos.y + TILE_HEIGHT + 5), new Sprite(ren->getSDL_Renderer(), std::string("res/Button/button.png"), NULL), 1);
	Button rotflipButton = Button(&board, vec2(board.extra->pos.x - 55, board.extra->pos.y + 25), new Sprite(ren->getSDL_Renderer(), std::string("res/Button/button-flip.png"), NULL), -1);


	while (!win->getShouldClose()) {
		win->update();
		ren->clear();

		#if AUTO_CLICKER
		//autocliker.exe
		SDL_Event nul;
		
		int w = rand() % WIN_WIDTH;
		int h = rand() % WIN_HEIGHT;
		win->setMousePos(vec2(w, h));
		button_press(*win, nul);
		//mm->playWalk();
		//autoclicker.exe end
		#endif

		//render bg
		bg->renderSprite(0, 0);
		ren->submit(&board);
		ren->submit(&bob);
		for (int i = 0; i < (board.size * 2) - 2; ++i) {
			ren->submit(slider[i]);
		}

		ren->submit(&rotButton);
		ren->submit(&rotflipButton);


		ren->draw();
		
	}

	delete ren;
	delete mm;
	delete win;

	return 0;

}

void key_press(Window& window, const SDL_Event& event) {
	if (event.key.keysym.sym == SDLK_ESCAPE) {
		window.setShouldClose(true);
	}
	else if (event.key.keysym.sym == SDLK_SPACE) {
		player->setTex(tex[(main_index++ % 4)]);
	}
}

void button_press(Window& window, const SDL_Event& event) {
	#if !AUTO_CLICKER
	if (event.button.button == SDL_BUTTON_LEFT) {
	#endif
		interact::Interactable* clickedTile = b->getTileOnClick(window.getMousePos());
		if (clickedTile != nullptr) {
			clickedTile->onClick(player);
			//soundEng->playWalk();
			//soundEng->playGet();
		}
	#if !AUTO_CLICKER
	}
	#endif
}

void mouse_move(Window& window, const SDL_Event& event) {
	window.setMousePos(vec2((float)event.motion.x, (float)event.motion.y));
}

void window_event(Window& window, const SDL_Event& event) {
	if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
		window.setWidth(event.window.data1);
		window.setHeight(event.window.data2);
		b->pos = b->centerBoard();
		b->update();
		for (int i = 0; i < (b->size * 2) - 2; ++i) {
			sliders[i]->pos = getSliderPos(b->size, i);
		}

		//TODO: FIX bug where the boundingboxes dont update with the box pos
		//TODO: FIX bug where on screen resize the player doesnt update to new position

	}
}



vec2 getSliderPos(int n, int i) {
	vec2 out = vec2();
	vec2 topLeft = b->pos;
	int side = getSliderSide(n, i);
	int mapped = (2 * i) + 1;
	int boardWidth = (n * TILE_WIDTH) + ((n - 1) * SPACER);
	int boardHeight = (n * TILE_HEIGHT) + ((n - 1) * SPACER);


	//mapped + 0.5 tileLengths and mapped spacers
	if (side == 0 || side == 2) {
		//move in x
		out.x = ((topLeft.x + (((float)(mapped + 0.5f) * TILE_WIDTH) + (mapped * SPACER))) - (SLIDER_WIDTH / 2) - ((side * boardWidth) - (side * TILE_WIDTH)));
		out.y = (side == 2) ? (topLeft.y  + (boardHeight + (SPACER * 2))) : (topLeft.y - (SLIDER_HEIGHT + (SPACER * 2)));
	}
	else {
		//move in y
		out.x = (side == 1) ? (topLeft.x + (boardWidth + (SPACER * 2))) : (topLeft.x - (SLIDER_WIDTH + (SPACER * 2)));
		out.y = ((topLeft.y + (((float)(mapped + 0.5f) * TILE_HEIGHT) + (mapped * SPACER))) - (SLIDER_HEIGHT/2) - ((side * boardHeight) - (side * TILE_HEIGHT)));
	}

	return out;

}

int getSliderSide(int n, int i) {
	int mapped = (2 * i) + 1;

	for (int a = 0; a < 4; ++a) {
		if (((a * n) - a) < mapped && mapped < ((a + 1)* n - (a + 1))) {
			return a;
		}
	}

	return -1;
}

int mapSliderToTile(int i) { //please ignore this garbage function. this will be fixed later.
	switch (i) {
	case 0:
		return 1;
	case 1:
		return 3;
	case 2:
		return 5;
	case 3:
		return 13;
	case 4:
		return 27;
	case 5:
		return 41;
	case 6:
		return 43;
	case 7:
		return 45;
	case 8:
		return 47;
	case 9:
		return 7;
	case 10:
		return 21;
	case 11:
		return 35;
	default:
		return -1;
	}
}
