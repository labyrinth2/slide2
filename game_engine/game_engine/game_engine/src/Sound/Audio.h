#pragma once
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <iostream>



namespace core { namespace sound {

		class Audio
		{
		public:

			Mix_Music *music = NULL;
			Mix_Chunk *sound1 = NULL;
			Mix_Chunk *sound2 = NULL;
			Mix_Music *secret_music = NULL;

			Audio();
			~Audio();
			void playMusic();
			//void stopMusic();
			void playWalk();
			void playGet();
		private:
			bool loadMedia();

		};

} }

