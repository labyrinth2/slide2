#include "Audio.h"

namespace core { namespace sound {

		Audio::Audio() {
			if (SDL_Init(SDL_INIT_AUDIO) < 0) {
				std::cout << "Audio did not Init!" << std::endl;
				exit(EXIT_FAILURE);
			}
			if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
				std::cout << "Audio did not open!" << std::endl;
				exit(EXIT_FAILURE);
			}
			if (!loadMedia()) {
				std::cout << "Audio did not load!" << std::endl;
				exit(EXIT_FAILURE);
			}
		}

		Audio::~Audio() {

		}

		bool Audio::loadMedia()
		{
			bool success = true;
			music = Mix_LoadMUS("res/Media/slideDOS.wav");
			if (music == NULL)
			{
				success = false;
			}
			sound1 = Mix_LoadWAV("res/Media/walk.wav");
			if (sound1 == NULL)
			{
				success = false;
			}
			sound2 = Mix_LoadWAV("res/Media/item_get.wav");
			if (sound2 == NULL)
			{
				success = false;
			}
			secret_music = Mix_LoadMUS("res/Media/SkeletonPartyMT32.wav");
			if (secret_music == NULL)
			{
				success = false;
			}
			return success;
		}

		void Audio::playMusic()
		{
			Mix_PlayMusic(secret_music, -1);
			return;
		}

		void Audio::playWalk()
		{
			if (Mix_Playing(1) == 1)
				Mix_HaltChannel(1);
			Mix_PlayChannel(1, sound1, 0);
			return;
		}
		void Audio::playGet()
		{
			if (Mix_Playing(2) == 1)
				Mix_HaltChannel(1);
			Mix_PlayChannel(2, sound2, 0);
			return;
		}
} }
