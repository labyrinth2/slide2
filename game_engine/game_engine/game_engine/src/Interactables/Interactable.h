#pragma once
#include "../Math/vec2.h"
#include "../Math/vec4.h"
#include "../../Labyrinth/Player.h"
#include <vector>



namespace core { namespace interact {

	class Interactable {
	public:

		math::vec4 boundingBox; //this is the 4 vals for a SDL_Rect which defines the hit box for the interactable
		static std::vector<Interactable*> interacts; //the list of all interactables (the instance should be added when created)

		virtual void onClick(slide::Player*) = 0; //the fucntion called when the interactable is clicked

		bool isIn(math::vec2 point) { //returns true if point is in the bounding box of the interactable
			return ((point.x >= boundingBox.x) && (point.x < boundingBox.x + boundingBox.z)) && ((point.y >= boundingBox.y) && (point.y < boundingBox.y + boundingBox.w));
		}

		static Interactable* getOnClick(math::vec2 point) { //finds the interactable in the static list that has the point in it
			for (int i = 0; i < Interactable::interacts.size(); ++i) {
				if (interacts[i]->isIn(point)) {
					return interacts[i];
				}
			}
			return nullptr;
		}

	};

} }
