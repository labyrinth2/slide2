#pragma once
#include <SDL2/SDL.h>
#include "Renderer.h"
#include "../Math/vec2.h"
#include <iostream>
#include <vector>
#include "../Interactables/Interactable.h"



namespace core { namespace graphics {
        
        class Window {
        private:
            
            const char* title;
            int width;
            int height;
            int winXPos = 10;
            int winYPos = 10;
            bool shouldClose = false;

			//std::vector<interact::Interactable*> objs;
            
            math::vec2 mousePos;
            
            SDL_Window* win;
			//Renderer* ren = nullptr;
            SDL_Event e;
            
            
        public:
            
			bool tempFlag = false;

            Window(const char* ptitle, int pwidth, int pheight);
           ~Window();
           
            bool getShouldClose() const ;
            bool setShouldClose(bool b);
           
			SDL_Window* getSDL_WindowPtr();
            //Renderer* getRendererPtr() ;
           
            void update();
           
            int getWidth() const ;
            int getHeight() const ;
			void setWidth(int new_width);
			void setHeight(int new_height);
			//void addObj(interact::Interactable* o);
			//interact::Interactable* getObj(int index);
           
            void setMousePos(const math::vec2& pos);
            const math::vec2& getMousePos();
           
            void setKeyCallback(void (*func)(Window& window, const SDL_Event& event));
            void setMouseButtonCallback(void (*func)(Window& window, const SDL_Event& event));
            void setMouseMotionCallback(void (*func)(Window& window, const SDL_Event& event));
			void setWindowEventCallback(void(*func)(Window& window, const SDL_Event& event));
           
        private:
           
            void (*key_callback)(Window& window, const SDL_Event& event);
            void (*mouse_button_callback)(Window& window, const SDL_Event& event);
            void (*mouse_motion_callback)(Window& window, const SDL_Event& event);
			void (*window_event_callback)(Window& window, const SDL_Event& event);

        };
        
        
        
} }