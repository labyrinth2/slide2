#pragma once
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <iostream>
#include <string>

namespace core { namespace graphics {

	class Sprite {
	private:

		SDL_Renderer* renderer;
		int width, height; //img dimensions
		SDL_Texture* img;
		SDL_Rect* clippingRect;
	
	public:

		Sprite();
		Sprite(SDL_Renderer* prenderer, std::string filepath, SDL_Rect* spriteClipping);
		~Sprite();

		int getWidth() const ;
		int getHeight() const ;

		void renderSprite(const int& x, const int& y);

	private:

		bool loadImg(const char* filepath);
		

	};

} }
