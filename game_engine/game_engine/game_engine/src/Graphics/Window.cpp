#include "Window.h"

namespace core { namespace graphics {
    
    Window::Window(const char* ptitle, int pwidth, int pheight) 
        : title(ptitle), width(pwidth), height(pheight)
    {
        if (SDL_Init(SDL_INIT_VIDEO) != 0) { //LOG THIS WHEN LOGGING IS AVAILABLE
            std::cout << "ERROR: SDL_Init() Failure!" << std::endl;
            std::cout << SDL_GetError() << std::endl;
            exit(EXIT_FAILURE);
        }

		winXPos = 40;
		winYPos = 40;

        win = SDL_CreateWindow(title, winXPos, winYPos, width, height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
        if (!win) {  //LOG THIS WHEN LOGGING IS AVAILABLE
            SDL_Quit();
            std::cout << "ERROR: Could not create window!" << std::endl;
            exit(EXIT_FAILURE);
        }

		//ren = new Renderer(win);
        
        mousePos = math::vec2(-1.0f, -1.0f);

		//objs = std::vector<interact::Interactable*>();

    }
    
    Window::~Window() {
        SDL_DestroyWindow(win);
        win = nullptr;
        SDL_Quit();
    }
    
    bool Window::getShouldClose() const {
        return shouldClose;
    }
    
    bool Window::setShouldClose(bool b) {
        shouldClose = b;
        return shouldClose;
    }
    
	SDL_Window* Window::getSDL_WindowPtr() {
		return win;
	}

    //Renderer* Window::getRendererPtr() {
    //    return ren;
    //}
    
    void Window::update() {
        while(SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                shouldClose = true;
                break; //exit loop as soon as this event is processed
            }
            else if (e.type == SDL_KEYDOWN) {
                key_callback(*this, e);
            }
            else if (e.type == SDL_MOUSEBUTTONDOWN) {
                mouse_button_callback(*this, e);
            }
            else if (e.type == SDL_MOUSEMOTION) {
                mouse_motion_callback(*this, e);
            }
			else if (e.type == SDL_WINDOWEVENT) {
				window_event_callback(*this, e);
			}
        }
    }
    
    int Window::getWidth() const {
        return width;
    }
    
    int Window::getHeight() const {
        return height;
    }

	void Window::setWidth(int new_width) {
		width = new_width;
	}

	void Window::setHeight(int new_height) {
		height = new_height;
	}

	//void Window::addObj(interact::Interactable* o) {
		//objs.push_back(o);
	//}

	//interact::Interactable* Window::getObj(int index) {
	//	return objs[index];
	//}
    
    void Window::setMousePos(const math::vec2& pos) {
        mousePos = pos;
    }
    
    const math::vec2& Window::getMousePos() {
        return mousePos;
    }
    
    void Window::setKeyCallback(void (*func)(Window& window, const SDL_Event& event)) {
        key_callback = func;
    }
    
    void Window::setMouseButtonCallback(void (*func)(Window& window, const SDL_Event& event)) {
        mouse_button_callback = func;
    }
    
    void Window::setMouseMotionCallback(void (*func)(Window& window, const SDL_Event& event)) {
        mouse_motion_callback = func;
    }

	void Window::setWindowEventCallback(void(*func)(Window& window, const SDL_Event& event)) {
		window_event_callback = func;
	}
  
} }

