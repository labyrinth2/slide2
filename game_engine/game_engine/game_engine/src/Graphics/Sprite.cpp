#include "Sprite.h"

namespace core { namespace graphics {

	Sprite::Sprite() {
		
	}

	Sprite::Sprite(SDL_Renderer* prenderer, std::string filepath, SDL_Rect* spriteClipping) {

		/*
			filepath: the filepath to the .png file
			spriteClipping: the SDL_Rect that is the portion of the img that you want to render. NULL if you want to render the whole img
		*/

		renderer = prenderer;
		if (!loadImg(filepath.c_str())) { //LOG THIS!
			std::cout << "Sprite could not load!" << std::endl;
		}
		clippingRect = spriteClipping;

	}

	Sprite::~Sprite() {
		SDL_DestroyTexture(img);
	}

	int Sprite::getWidth() const { return width; }
	int Sprite::getHeight() const { return height; }

	void Sprite::renderSprite(const int& x, const int& y) {
		SDL_Rect renderQuad = { x, y, width, height };
		if (clippingRect != nullptr) {
			renderQuad.w = clippingRect->w;
			renderQuad.h = clippingRect->h;
		}
		SDL_RenderCopy(renderer, img, clippingRect, &renderQuad);
	}

	bool Sprite::loadImg(const char* filepath) {

		img = NULL;
		SDL_Surface* surf = IMG_Load(filepath);
		if (!surf) {
			std::cout << "Unable to load img: " << filepath << "\nSDL_Error: " << IMG_GetError();
		}
		else {
			SDL_SetColorKey(surf, SDL_TRUE, SDL_MapRGB(surf->format, 0, 0xFF, 0xFF));
			img = SDL_CreateTextureFromSurface(renderer, surf);
			if (img == NULL) {
				std::cout << "Unable to create texture from " << filepath << "\nSDL Error: " << SDL_GetError();
			}
			else {
				//Get image dimensions
				width = surf->w;
				height = surf->h;
			}

			SDL_FreeSurface(surf);

		}

		return img != NULL;

	}

} }
