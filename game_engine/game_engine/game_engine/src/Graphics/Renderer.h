#pragma once
#include <SDL2/SDL.h>
#include "../Math/vec4.h"
#include "Window.h"
#include "Renderable.h"
#include <vector>
#include <queue>

namespace core { namespace graphics {

    class Renderer {
    private:
    
        SDL_Renderer* rend;

		//std::priority_queue<Renderable> render_queue;
        
        math::vec4 color = math::vec4(0xFF, 0xFF, 0xFF, 0xFF);
    
    public:
   
        Renderer(SDL_Window* ref_win);
        Renderer(SDL_Renderer* prend);
       ~Renderer();
       
       void clear() const ;
       void draw() ;

	   SDL_Renderer* getSDL_Renderer();

	   void submit(Renderable* entity);
	   
       const math::vec4& getRenderColor() ;

       
       void drawRect(const int& xpos, const int& ypos, const int& width, const int& height, const math::vec4& rgba);
    
    };
        
} }