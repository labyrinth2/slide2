#pragma once
#include <SDL2/SDL.h>
#include "../Math/vec2.h"


namespace core { namespace graphics {

	class Renderable {
	public:

		math::vec2 pos; //position in window (0,0) is top left

		virtual void draw(SDL_Renderer* renderer) = 0;
	
	};

} }
