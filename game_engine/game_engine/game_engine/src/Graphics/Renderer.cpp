#include "Renderer.h"

namespace core { namespace graphics {
  
    //////////////////////////////////////////
    //
    //    PUBLIC FUNCTIONS
    //
    //////////////////////////////////////////

    Renderer::Renderer(SDL_Window* ref_win) {
        rend = SDL_CreateRenderer(ref_win, -1, SDL_RENDERER_ACCELERATED);
        if (!rend) {  //LOG THIS WHEN LOGGING IS AVAILABLE
            SDL_Quit();
            std::cout << "ERROR: Could not create renderer!" << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    
    Renderer::Renderer(SDL_Renderer* prend)
        : rend(prend)
    {
       
        if (!rend) {  //LOG THIS WHEN LOGGING IS AVAILABLE
            SDL_Quit();
            std::cout << "ERROR: Could not create renderer!" << std::endl;
            exit(EXIT_FAILURE);
        }
     
    }
    
    Renderer::~Renderer() {
        SDL_DestroyRenderer(rend);
        rend = nullptr;
    }
    
    void Renderer::clear() const {
        SDL_SetRenderDrawColor(rend, 0x00, 0x00, 0x00, 0x00);
        SDL_RenderClear(rend);
		SDL_SetRenderDrawColor(rend, 0xff, 0xff, 0xff, 0xff);
    }
    
    void Renderer::draw() {   
        SDL_RenderPresent(rend);
    }

	SDL_Renderer* Renderer::getSDL_Renderer() {
		return rend;
	}

	void Renderer::submit(Renderable* entity) {
		entity->draw(rend);
	}

    const math::vec4& Renderer::getRenderColor() {
        return color;
    }
    
    void Renderer::drawRect(const int& xpos, const int& ypos, const int& width, const int& height, const math::vec4& rgba) {
        
        SDL_Rect rect = { xpos, ypos, width, height } ;
        
        SDL_SetRenderDrawColor(rend, (Uint8)rgba.x, (Uint8)rgba.y, (Uint8)rgba.z, (Uint8)rgba.w);
        
        if (SDL_RenderFillRect(rend, &rect) < 0) {
            std::cout << "Renderer Error: error on drawRect(" << xpos << ", " 
            << ypos << ", " << width << ", " << height << ", " << rgba  << ")" 
            << "\nSDL_GetError() returned -> "  << SDL_GetError() << std::endl;
        }
        
        SDL_SetRenderDrawColor(rend, 0xFF, 0xFF, 0xFF, 0xFF);
        
    }
    
    //////////////////////////////////////////
    //
    //    PRIVATE FUNCTIONS
    //
    //////////////////////////////////////////
    
} }