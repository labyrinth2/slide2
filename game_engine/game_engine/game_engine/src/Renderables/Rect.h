#pragma once
#include <SDL2/SDL.h>
#include "../Graphics/Renderable.h"
#include "../Math/vec3.h"
#include "../Math/vec4.h"

namespace core { namespace graphics {

	class Rect : public Renderable {
	public:

		int width, height;
		math::vec4 color;


		Rect(const math::vec2& ppos, int pwidth, int pheight) {
			pos = ppos;
			width = pwidth;
			height = pheight;
		}

		void setRectColor(const math::vec4& new_color) {
			color = new_color;
		}

		void draw(SDL_Renderer* renderer) {
			SDL_Rect rect = { (int)pos.x, (int)pos.y, width, height };
			SDL_SetRenderDrawColor(renderer, (Uint8)color.x, (Uint8)color.y, (Uint8)color.z, (Uint8)color.w);
			if (SDL_RenderFillRect(renderer, &rect) < 0) {
				std::cout << "Renderer Error" << std::endl;
			}
			SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
		}

	};

} }
